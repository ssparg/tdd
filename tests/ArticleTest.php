<?php

use App\Article;
use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase
{

    protected $article;


    public function setUp(): void
    {
        $this->article = new Article;
    }

    public function testTitleIsEmptyByDefault()
    {
        $this->assertEmpty($this->article->title);
    }

    public function testSlugIsEmptyWithNoTitle()
    {
        $this->assertSame($this->article->getSlug(), "");
    }
    /*
    public function testSlugHasSpacesReplacedByUnderscores()
    {
        $this->article->title = "An example article";
        $this->assertEquals($this->article->getSlug(), "An_example_article");
    }

    public function testSlugHasWhiteSpaceReplacedByUnderscore()
    {
        $this->article->title = "An           example       \n        article";
        $this->assertEquals($this->article->getSlug(), "An_example_article");
    }

    public function testSlugDoesNotStartOrEndWithAnUnderscore()
    {
        $this->article->title = " An example article ";
        $this->assertEquals($this->article->getSlug(), "An_example_article");
    }

    public function testSlugHasNonWordCharactersRemoved()
    {
        $this->article->title = "Read! Read! Read!";
        $this->assertEquals($this->article->getSlug(), "Read_Read_Read");
    }
    */
    public function titleProvider()
    {
        return [
            ["An example article", "An_example_article"],
            ["An           example       \n        article", "An_example_article"],
            [" An example article ", "An_example_article"],
            ["Read! Read! Read!", "Read_Read_Read"]
        ];
    }

    /**
     * @dataProvider titleProvider
     */
    public function testSlug($title, $slug)
    {
        $this->article->title = $title;
        $this->assertEquals($this->article->getSlug(), $slug);
    }
}
